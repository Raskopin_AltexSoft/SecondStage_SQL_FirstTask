DECLARE @iterator int = 1;
DECLARE @sql nvarchar(1000);
DECLARE @tab AS TABLE (t1 float);
DECLARE @VAL float;
DECLARE @CURSOR CURSOR 
DECLARE @RESULT float = 1;

WHILE (SELECT COL_NAME(OBJECT_ID('dbo.Table_1'),@iterator)) is not NULL
BEGIN
	IF @iterator % 2 = 0
	set @sql = N'select ' + (SELECT COL_NAME(OBJECT_ID('dbo.Table_1'),@iterator)) + ' from dbo.Table_1';
	set @iterator = @iterator+1;
	insert into @tab exec sp_executesql @sql;
END

SET @CURSOR = CURSOR SCROLL
FOR 
	select * from @tab
OPEN @CURSOR
	FETCH NEXT FROM @CURSOR INTO @VAL
WHILE @@FETCH_STATUS = 0
BEGIN
	set @RESULT = @RESULT * @VAL;
FETCH NEXT FROM @CURSOR INTO @VAL
END
CLOSE @CURSOR

print @RESULT