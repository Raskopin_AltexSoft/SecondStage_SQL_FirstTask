DECLARE @iterator int = 1;
DECLARE @sql nvarchar(1000);
DECLARE @tab AS TABLE (t1 int);

WHILE (SELECT COL_NAME(OBJECT_ID('dbo.Table_1'),@iterator)) is not NULL
BEGIN
set @sql = N'select top(1) row from (select ROW_NUMBER() over(order by (SELECT NULL)) as row_number, ' 
+ (SELECT COL_NAME(OBJECT_ID('dbo.Table_1'),@iterator)) + ' as row from dbo.Table_1) as result where row_number = ' + cast(@iterator as nvarchar(1000));
insert into @tab exec sp_executesql @sql;
set @iterator = @iterator+1;
END
select SUM(t1) from @tab
